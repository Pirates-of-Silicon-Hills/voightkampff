echo ">>setWindow.sh"
xdotool windowsize `xdotool search --onlyvisible -name firefox` 2280 1600
xdotool windowmove `xdotool search --onlyvisible -name firefox` 0 0
sleep 1
xdotool mousemove 2240 80 click 1 #Close Firefox properly
sleep 2
xdotool mousemove 4000 2000 # make sure mouse is not there when we take screenshot to check for firefox errors
sleep 5
firefox "https://news.ycombinator.com/login" &
sleep 5
xdotool mousemove 1000 1000 click 1 # weird bug with password box moving toward 0,0 
sleep 0.1
xdotool windowsize `xdotool search --onlyvisible -name firefox` 2280 1600
xdotool windowmove `xdotool search --onlyvisible -name firefox` 0 0
sleep 1 