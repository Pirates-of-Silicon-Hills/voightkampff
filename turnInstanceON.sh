#!/bin/bash

declare -a instances
instances[0]="aws ec2 start-instances  --instance-ids i-09be868e7251ff275  --region us-east-1"
instances[1]="aws ec2 start-instances  --instance-ids i-018a71a8b62711ac2  --region us-east-2"
instances[2]="aws ec2 start-instances  --instance-ids i-034c955aab1114687  --region us-west-1"
instances[3]="aws ec2 start-instances  --instance-ids i-043855242f72de89d  --region us-west-2"
instances[4]="aws ec2 start-instances  --instance-ids i-01acfd1bd855c7883  --region ap-northeast-2"
instances[5]="aws ec2 start-instances  --instance-ids i-02b0d141e948a1136  --region ap-southeast-1"
instances[6]="aws ec2 start-instances  --instance-ids i-08310b97b301f6cd0  --region ap-southeast-2"
instances[7]="aws ec2 start-instances  --instance-ids i-06c938bc5e8370d40  --region eu-central-1"
instances[8]="aws ec2 start-instances  --instance-ids i-00c9d6ac2ff058c97  --region af-south-1"  
instances[9]="aws ec2 start-instances --instance-ids i-01dbb61c54d755309  --region ap-east-1"  
instances[10]="aws ec2 start-instances --instance-ids i-03f25a7e427fe6269  --region ap-south-1"  
instances[11]="aws ec2 start-instances --instance-ids i-0583d39963763bd91  --region ap-northeast-1"  
instances[12]="aws ec2 start-instances --instance-ids i-05bceff97897797f8  --region ca-central-1"  
instances[13]="aws ec2 start-instances --instance-ids i-04bc5c446108c364b  --region eu-west-1"  
instances[14]="aws ec2 start-instances --instance-ids i-02c8ccada959c93b0  --region eu-west-2"  
instances[15]="aws ec2 start-instances --instance-ids i-0739d160bcd3366ca  --region eu-south-1"  
instances[16]="aws ec2 start-instances --instance-ids i-034dac54659ed8d3c  --region eu-west-3"  
instances[17]="aws ec2 start-instances --instance-ids i-00f05adc1319e669f  --region eu-north-1"  
instances[18]="aws ec2 start-instances --instance-ids i-00a50e0e47faf9545  --region me-south-1"  
instances[19]="aws ec2 start-instances --instance-ids i-0429661f7f9fe75fd  --region sa-east-1"


declare -a getDNS
getDNS[0]="aws ec2 describe-instances --instance-ids i-09be868e7251ff275   --region us-east-1        --query"  
getDNS[1]="aws ec2 describe-instances --instance-ids i-018a71a8b62711ac2   --region us-east-2        --query"  
getDNS[2]="aws ec2 describe-instances --instance-ids i-034c955aab1114687   --region us-west-1        --query"  
getDNS[3]="aws ec2 describe-instances --instance-ids i-043855242f72de89d   --region us-west-2        --query" 
getDNS[4]="aws ec2 describe-instances --instance-ids i-01acfd1bd855c7883   --region ap-northeast-2   --query" 
getDNS[5]="aws ec2 describe-instances --instance-ids i-02b0d141e948a1136   --region ap-southeast-1   --query" 
getDNS[6]="aws ec2 describe-instances --instance-ids i-08310b97b301f6cd0   --region ap-southeast-2   --query" 
getDNS[7]="aws ec2 describe-instances --instance-ids i-06c938bc5e8370d40   --region eu-central-1     --query" 
getDNS[8]="aws ec2 describe-instances --instance-ids i-00c9d6ac2ff058c97   --region af-south-1       --query"  
getDNS[9]="aws ec2 describe-instances --instance-ids i-01dbb61c54d755309  --region ap-east-1        --query"  
getDNS[10]="aws ec2 describe-instances --instance-ids i-03f25a7e427fe6269  --region ap-south-1       --query"  
getDNS[11]="aws ec2 describe-instances --instance-ids i-0583d39963763bd91  --region ap-northeast-1   --query"  
getDNS[12]="aws ec2 describe-instances --instance-ids i-05bceff97897797f8  --region ca-central-1     --query"  
getDNS[13]="aws ec2 describe-instances --instance-ids i-04bc5c446108c364b  --region eu-west-1        --query"  
getDNS[14]="aws ec2 describe-instances --instance-ids i-02c8ccada959c93b0  --region eu-west-2        --query"  
getDNS[15]="aws ec2 describe-instances --instance-ids i-0739d160bcd3366ca  --region eu-south-1       --query"  
getDNS[16]="aws ec2 describe-instances --instance-ids i-034dac54659ed8d3c  --region eu-west-3        --query"  
getDNS[17]="aws ec2 describe-instances --instance-ids i-00f05adc1319e669f  --region eu-north-1       --query"  
getDNS[18]="aws ec2 describe-instances --instance-ids i-00a50e0e47faf9545  --region me-south-1       --query"  
getDNS[19]="aws ec2 describe-instances --instance-ids i-0429661f7f9fe75fd  --region sa-east-1        --query"

declare -a getStatus 
getStatus[0]="aws ec2 describe-instance-status --instance-ids i-09be868e7251ff275   --region us-east-1        --query"  
getStatus[1]="aws ec2 describe-instance-status --instance-ids i-018a71a8b62711ac2   --region us-east-2        --query"  
getStatus[2]="aws ec2 describe-instance-status --instance-ids i-034c955aab1114687   --region us-west-1        --query"  
getStatus[3]="aws ec2 describe-instance-status --instance-ids i-043855242f72de89d   --region us-west-2        --query" 
getStatus[4]="aws ec2 describe-instance-status --instance-ids i-01acfd1bd855c7883   --region ap-northeast-2   --query" 
getStatus[5]="aws ec2 describe-instance-status --instance-ids i-02b0d141e948a1136   --region ap-southeast-1   --query" 
getStatus[6]="aws ec2 describe-instance-status --instance-ids i-08310b97b301f6cd0   --region ap-southeast-2   --query" 
getStatus[7]="aws ec2 describe-instance-status --instance-ids i-06c938bc5e8370d40   --region eu-central-1     --query" 
getStatus[8]="aws ec2 describe-instance-status --instance-ids i-00c9d6ac2ff058c97   --region af-south-1       --query"  
getStatus[9]="aws ec2 describe-instance-status --instance-ids i-01dbb61c54d755309  --region ap-east-1        --query"  
getStatus[10]="aws ec2 describe-instance-status --instance-ids i-03f25a7e427fe6269  --region ap-south-1       --query"  
getStatus[11]="aws ec2 describe-instance-status --instance-ids i-0583d39963763bd91  --region ap-northeast-1   --query"  
getStatus[12]="aws ec2 describe-instance-status --instance-ids i-05bceff97897797f8  --region ca-central-1     --query"  
getStatus[13]="aws ec2 describe-instance-status --instance-ids i-04bc5c446108c364b  --region eu-west-1        --query"  
getStatus[14]="aws ec2 describe-instance-status --instance-ids i-02c8ccada959c93b0  --region eu-west-2        --query"  
getStatus[15]="aws ec2 describe-instance-status --instance-ids i-0739d160bcd3366ca  --region eu-south-1       --query"  
getStatus[16]="aws ec2 describe-instance-status --instance-ids i-034dac54659ed8d3c  --region eu-west-3        --query"  
getStatus[17]="aws ec2 describe-instance-status --instance-ids i-00f05adc1319e669f  --region eu-north-1       --query"  
getStatus[18]="aws ec2 describe-instance-status --instance-ids i-00a50e0e47faf9545  --region me-south-1       --query"  
getStatus[19]="aws ec2 describe-instance-status --instance-ids i-0429661f7f9fe75fd  --region sa-east-1        --query"

#n=$RANDOM
#n=$i
${instances[$1]}

echo "Booting instance"

state="pending"
while [ "$state" != "running" ]
do
	command=" ${getDNS[$1]} 'Reservations[0].Instances[0].State.Name' > state.txt"
	eval $command
	echo $state
	if [[ "$state" == "stopped" ]]; then
	   ${instances[$1]}
	fi
	#echo $command
	sleep 5
	state=`cat state.txt`
	temp="${state%\"}"
	state="${temp#\"}"
done
rm state.txt

#echo "Status checks 1/2"
#state="initializing"
#while [ "$state" != "ok" ]
#do
#	command=" ${getStatus[$1]} 'InstanceStatuses[0].InstanceStatus.Status' > state.txt"
#	eval $command
#	echo $state
#
#	#echo $command
#	sleep 5
#	state=`cat state.txt`
#	temp="${state%\"}"
#	state="${temp#\"}"
#done
#rm state.txt
#
#echo "Status checks 2/2"
#state="initializing"
#while [ "$state" != "ok" ]
#do
#	command=" ${getStatus[$1]} 'InstanceStatuses[0].SystemStatus.Status' > state.txt"
#	eval $command
#	echo $state
#
#	#echo $command
#	sleep 5
#	state=`cat state.txt`
#	temp="${state%\"}"
#	state="${temp#\"}"
#done
#rm state.txt

echo "instance ready"



