echo ">>vision API.sh"

xdotool windowsize `xdotool search --onlyvisible -name firefox` 3580 2500
xdotool windowmove `xdotool search --onlyvisible -name firefox` 100 50
sleep 2
xdotool mousemove 300 1800 click 1 # Focus

for i in {1..15}
do
   xdotool key Up
done
sleep 2
for i in {1..15}
do
   xdotool key Down
done

waitForPuzzle=0
puzzleLoaded=0
while [ $puzzleLoaded -eq 0 ] && [ $waitForPuzzle -le 10 ]
do	
	maim -g 1700x400+700+1200 try.png
	STR=$(tesseract try.png stdout)	

	SUB='from your computer'
	if [[ "$STR" == *"$SUB"* ]]; then
	  	puzzleLoaded=1
	  	echo "Upload window loaded. Not blocked"
  	else
		echo "Waiting windows to load"
	fi
	((waitForPuzzle=waitForPuzzle+1))
	sleep 0.5
done

if [ ! $puzzleLoaded -eq 1 ]; then
	echo "ERROR: Google has blocked us for having a low V3 score. Probably 0.1"
else

	sleep 2
	xdotool mousemove 1600 1600 click 1 # Open Select Image pane
	sleep 1
	xdotool mousemove 700 820 click 1 # Click on Pictures 
	sleep 2
	xdotool mousemove 1600 1600 click 1 # Focus on pictures pane
	sleep 1
	select=$RANDOM
	select=$(($select%6))
	echo $select
	for (( c=0; c<=$select; c++ ))
	do  
	   xdotool key Down
	done
	sleep 0.5
	xdotool key Return # select image
	sleep 1.5


	nx=$RANDOM
	ny=$RANDOM
	dx=$((3530+$nx%100)) 
	dy=$((2400+$ny%100))
	x=$((150-$((($nx%100)/2))))
	y=$((150-$((($ny%100)))))
	echo $x $y

	xdotool windowsize `xdotool search --onlyvisible -name firefox` $dx $dy  # 2980 2300
	xdotool windowmove `xdotool search --onlyvisible -name firefox` $x 0

	waitForPuzzle=0
	puzzleLoaded=0
	while [ $puzzleLoaded -eq 0 ] && [ $waitForPuzzle -le 10 ]
	do	
		maim -g 350x160+1520+2300 captcha.png
		STR=$(tesseract captcha.png stdout)	

		SUB='not a robot'
		if [[ "$STR" == *"$SUB"* ]]; then
		  	puzzleLoaded=1
		  	echo "Puzzle box Loaded"
	  	else
			echo "Waiting for tick to load"
		fi
		((waitForPuzzle=waitForPuzzle+1))
		sleep 0.5
	done

	sleep 1
	xdotool mousemove 1490 2350 click 1                         # Click on tick
	sleep 2
	echo "SCORE OVERRIDEN SO ITS ALWAYS  0.3 SO THAT IT NEVER EVEN TRIES 4X4"
	time ./main_visionAPI.sh 1355 900 0 3
fi




