# --------------------------------------------------------------------------------
# main.sh
#
# Author:  Ozymandias
#          [ef3add5f32de7d98ead2e7bb6dfad933298fc99e6b910fa3678e3ba54e7303df]
# Github:  pirates-of-silicon-hills/VoightKampff
# Date:    November 2019-July 2020
# License: MIT 
#
#
#     I'm not a comic book villain. Do you seriously
#     think I would explain my master stroke to you 
#     if there were even the slightest possibility 
#     you could affect the outcome?           
#
#                       I triggered it 35 minutes ago
#                       				-Adrian Veidt
#
# --------------------------------------------------------------------------------



echo "████████████████"
echo "██          ██  ██" 
echo "██          ██    ██"
echo "██  ████  ██████  ██"
echo "██  ████  ██████████"
echo "██        ██████  ██"
echo "██                ██"
echo "██                ██"
echo "██  ██        ██  ██"
echo "██  ████████████  ██"
echo "██                ██"  
echo "████████████████████"
echo ""
echo "PIRATES OF SILICON HILLS 2020"
echo "MIT License"

. ./checkScore.sh

for orbit in {0..1000}
do
	for dns in {0..19}
	do
		echo "Region: " $dns
		./switchDNS.sh $dns
		get_score
		score=$?
		if [ $score -ge 3 ]; then
			./setWindow_SO.sh
			./SO.sh $dns
			time ./main.sh 870 725 0 $score
			xdotool windowsize `xdotool search --onlyvisible -name firefox` 2280 1600
			xdotool windowmove `xdotool search --onlyvisible -name firefox` 0 0
			sleep 1
			xdotool mousemove 2240 80 click 1 #Close Firefox properly 
			sleep 5
			#get_score
		else
			echo "Skipping due to low score on V3 captcha"
		fi
	done
done


