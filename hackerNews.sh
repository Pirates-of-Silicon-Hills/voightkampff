echo ">>Hacker News.sh"

n=$RANDOM
x=$((2780-$n%60))
y=$((2100-$n%60))
echo $x $y

xdotool windowsize `xdotool search --onlyvisible -name firefox` $x $y  # 2280 1600
xdotool windowmove `xdotool search --onlyvisible -name firefox` 0 0
sleep 1
xdotool mousemove 250 890 click 1                                      # Click on username field
xdotool type `cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 14 | head -n 1`
sleep 1
xdotool mousemove 1000 1000 click 1                                    # Click outside field to get that password stuff to go
sleep 1
xdotool mousemove 250 970 click 1 	 								   # Click on password field
sleep 1
xdotool type `cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 14 | head -n 1`
sleep 1
xdotool mousemove 230 1080 click 1 				   					   # Click on submit button
sleep 1

waitForPuzzle=0
puzzleLoaded=0
while [ $puzzleLoaded -eq 0 ] && [ $waitForPuzzle -le 10 ]
do
	xdotool mousemove 1000 1000 click 1                    			   # Get focus	
	maim -g 380x180+20+740 boxLoaded.png
	STR=$(tesseract boxLoaded.png stdout)	

	SUB='not a robot'
	if [[ "$STR" == *"$SUB"* ]]; then
	  	puzzleLoaded=1
	  	echo "Puzzle Loaded"
  	else
		echo "Waiting for tick to load"
	fi
	((waitForPuzzle=waitForPuzzle+1))
	sleep 0.5
done

echo "Box loaded"
xdotool mousemove 75 840 click 1 									   # Click on tick
sleep 2