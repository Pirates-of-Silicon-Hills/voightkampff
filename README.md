![You are retired](https://bitbucket.org/Pirates-of-Silicon-Hills/voightkampff/raw/5aab6c3fa86df0418d34c2ea42f167a75b0f3c8e/kowalski.jpg)



# Project Voight-Kampff

Beating Google's reCaptcha using AWS Rekognition. Part of project Touch-Captcha. I did this because I cannot promote a better Captcha without first beating the industry standard. 

Nothing special here. Credit goes to the ML researchers who developed the image classification technologies readily available today, either via the Google Vision API or AWS Rekognition.

Voight-Kampff comes from the movie Blade Runner (1982). It is the test used by Blade Runners to tell a Replicant(synthetic human/android) from a human being.




### You will need:

- Google GCP account (The virtual machines I use are hosted in GCP)
- AWS account (Proxies)



### To get started watch how-to videos

Watch this video that shows everything from start to finish: https://www.youtube.com/watch?v=qt2jE-eI5sg&ab_channel=PiratesofSiliconHills

curl "https://bitbucket.org/Pirates-of-Silicon-Hills/voightkampff/raw/9fa5cb696523d5314bd90926445423bd0e538d44/setup.sh" --output setup.sh

chmod u+r+x setup.sh

./setup.sh



### Past Puzzles

Every puzzle you see in my demonstration videos has been saved as an image with a unique name as identifier. You can download all the images here: https://drive.google.com/open?id=18b0HxyOsLP6AZMpF1-DNITrGvFBGkYND 



### In case you plan to exploit this...

I informed Google already. Like 4 times. I told them EVERYTHING I planned to do weeks before you read this. I am just trying to promote the Captcha I developed by first beating the industry standard. I am not trying to play Mr. Hacker and neither should you.



### Author

**Ozymandias** 
[@worlds-smartest-man]
      
![David](https://bitbucket.org/Pirates-of-Silicon-Hills/voightkampff/raw/5aab6c3fa86df0418d34c2ea42f167a75b0f3c8e/david.gif)

Formalities

- Former researcher at MIT CSAIL
- Former Undergraduate Research Fellow. NSF funded
- Employed at 4 different labs over the course of my undergraduate studies 

### Trivia

- Pirates of Silicon Hills is a word play on the classic movie Pirates of Silicon Valley(1999). Silicon Hills refers to the Austin, TX tech scene

### License

MIT


